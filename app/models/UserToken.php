<?php

class UserToken extends \Eloquent
{
    protected $fillable = ["user_id", "token"];
    protected $table = "user_token";

    protected function user()
    {
        return $this->belongsTo('User');
    }

    public function getToken()
    {
        return $this->token;
    }

}