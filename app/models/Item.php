<?php

class Item extends \Eloquent {
	protected $fillable = [];
    protected $guarded = array();
    protected $table = "item";

    public function users() {
        return $this->belongsToMany('User');
    }
}