<?php

class ItemTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('item')->delete();

        $faker = Faker\Factory::create();

        for ($i = 0; $i < 100; $i++) {
            for ($i = 0; $i < 100; $i++) {

                $imageNum = rand(0,176);
                if($imageNum <10){
                    $imageNum = "00".$imageNum;
                } elseif($imageNum <100){
                    $imageNum = "0".$imageNum;
                }

                $user = Item::create(
                    array(
                        'name' => $faker->word,
                        'image' => "files/butra-" . $imageNum . ".jpg",
                        'description' => $faker->sentence(),
                    )
                );
            }
        }
    }
}