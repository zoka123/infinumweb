<?php

class ItemUserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('item_user')->delete();

        for ($i = 1; $i <= 100; $i++) {
            for ($j = 1; $j <= 10; $j++) {
                DB::table('item_user')->insert(array("user_id" => $i, "item_id" => rand(1, 100)));
            }
        }
    }
}