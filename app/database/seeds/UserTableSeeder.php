<?php

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('user')->delete();

        // Few users
        User::create(array(
            'email' => "zokadgo@gmail.com",
            'name' => "Zoran Antolović",
            'password' => Hash::make("123456"),
        ));

        $faker = Faker\Factory::create();

        for ($i = 0; $i < 100; $i++) {
            $user = User::create(array(
                'name' => $faker->userName . " " . $faker->firstName,
                'email' => $faker->email,
                'password' => Hash::make($faker->word),
            ));
        }
    }
}