<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'item',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('image');
                $table->string('description');
                $table->timestamps();

                $table->integer('dibsed_by')->unsigned()->nullable();
                $table->foreign('dibsed_by')->references('id')->on('user')->onDelete(DB::raw('set null'));

            }
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('item');
    }

}
