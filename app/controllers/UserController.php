<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends \BaseController
{

    private $userId = null;

    public function __construct()
    {
        App::error(
            function (ModelNotFoundException $e) {
                return Response::make(json_encode(array("success" => false, "msg" => $e->getMessage())));
            }
        );

        $this->beforeFilter("csrf", array("only" => array("register", "browserLogin")));
        $this->beforeFilter("auth", array("only" => array("wishlist","userWishlist")));
        $this->beforeFilter('api_auth', array('except' => array('login', 'register', "browserLogin", "browserLogout", "wishlist","userWishlist")));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return User::all();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return Users::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function login()
    {
        $user = array("password" => Input::get("password"), "email" => Input::get("email"));

        if (Auth::attempt($user)) {
            $userInstance = User::where('email', $user['email'])->get(array('id'))->first();
            $token = new UserToken;
            $token->fill(array("user_id" => $userInstance->getKey(), "token" => md5(time() . $user['password'])));
            $token->save();

            return json_encode(array("success" => true, "token" => $token->token, "msg" => "User logged in"));
        } else {
            return json_encode(array("success" => false, "msg" => "User doesn't exist"));
        }
    }

    public function browserLogin()
    {
        $user = array("password" => Input::get("password"), "email" => Input::get("email"));

        if (Auth::attempt($user, false)) {
            return json_encode(array("success" => true, "msg" => "User logged in"));
        } else {
            return json_encode(array("success" => false, "msg" => array("User doesn't exist")));
        }
    }

    public function register()
    {
        $rules = array(
            'name' => 'required',
            'password' => 'required|confirmed',
            'email' => 'required|email|unique:user'
        );

        $validator = Validator::make(Input::all(), $rules);

        if (!$validator->fails()) {
            $pass = Input::get("password");
            $pass = Hash::make($pass);
            Input::merge(array("password" => $pass));
            User::create(Input::except("password_confirmation"));

            return json_encode(array("success" => true, "msg" => "User is registered"));
        } else {
            $msg = "";
            foreach ($validator->messages()->getMessages() as $k => $m) {
                foreach ($m as $error) {
                    $msg .= "<p>{$error}</p>";

                }
            }

            return json_encode(array("success" => false, "msg" => $msg));
        }
    }

    public function logout()
    {
        UserToken::where("user_id", Input::get("userId"))->delete();

        return json_encode(array("success" => true, "msg" => "User logged out"));
    }

    public function browserLogout()
    {

        Auth::logout();

        return Redirect::action("HomeController@index");
    }

    public function items()
    {
        $result = DB::table('item')->join('item_user', 'item.id', '=', 'item_user.item_id')->join(
            'user',
            'item_user.user_id',
            '=',
            'user.id'
        )->where('user.id', Input::get('userId'))->select(array('item.id','item.description','item.image', 'item.name'))->orderBy(
                "item_user.created_at",
                "desc"
            )->get();

        foreach ($result as &$item) {
            $item->image = url("/assets/img/") . "/" . $item->image;
        }

        return $result;
    }

    public function wishlist()
    {
        $result = DB::table('item')->join('item_user', 'item.id', '=', 'item_user.item_id')->join(
            'user',
            'item_user.user_id',
            '=',
            'user.id'
        )->where('user.id', Auth::getUser()->getKey())->select(array('item.dibsed_by','user.name as username','item.description','item.created_at','item.id as itemId','item.image', 'item.name'))->orderBy(
                "item_user.created_at",
                "desc"
            )->get();

        foreach ($result as &$item) {
            $item->image = url("/assets/img/") . "/" . $item->image;
        }

        return View::make('wishlist')->with('items',$result);
    }

    public function userWishlist($id){
        $result = DB::table('item')->join('item_user', 'item.id', '=', 'item_user.item_id')->join(
            'user',
            'item_user.user_id',
            '=',
            'user.id'
        )->where('user.id', $id)->select(array('item.dibsed_by','user.name as username','item.description','item.created_at','item.id as itemId','user.id as userId','item.image', 'item.name'))->orderBy(
                "item_user.created_at",
                "desc"
            )->get();

        foreach ($result as &$item) {
            $item->image = url("/assets/img/") . "/" . $item->image;
        }

        return View::make('user-wishlist')->with('items',$result);
    }




}
