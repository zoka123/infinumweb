<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		$items = DB::table("item")->join("item_user","item.id","=","item_user.item_id")->join("user","user.id","=","item_user.user_id")->select(array('item.dibsed_by',"item.id as itemId", "user.id as userId", "user.name as username","item.name","item.image","item.description","item_user.created_at"))->orderBy("item_user.created_at","desc")->limit(20)->get();

        foreach($items as &$item){
            $item ->image = url("/assets/img/")."/".$item -> image;
        }

        return View::make('home')->with('items',$items);
	}

}
