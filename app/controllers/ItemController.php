<?php

class ItemController extends \BaseController
{

    public function __construct()
    {
        $this->beforeFilter('api_auth', array('except' => array('dibs')));
        $this->beforeFilter("auth", array("only" => array("dibs")));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($itemId = Input::get("itemId")) {
            if ($itemInstance = Item::find($itemId)) {

                if (
                DB::table("item_user")->where("user_id", "=", Input::get("userId"))->where("item_id", "=" ,$itemId)->get() ||
                DB::table('item_user')->insert(array("user_id" => Input::get("userId"), "item_id" => $itemId, "created_at" => date( 'Y-m-d H:i:s', time() )))) {
                    return json_encode(array("success" => true, "msg" => "Item added"));
                } else {
                    return json_encode(array("success" => false, "msg" => "Error adding item"));
                }
            } else {
                return json_encode(array("success" => false, "msg" => "Item doesn't exist"));
            }
        }

        return json_encode(array("success" => false, "msg" => "No item"));
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show()
    {
        $id = Input::get("itemid");

        $item =  Item::find($id)->first();
        $item -> image =   url("/assets/img/") . "/" . $item->image;
        return $item;
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function dibs(){
        $itemId = Input::get("id");
        $userId = Auth::getUser()->getKey();
        $item = Item::find($itemId);
        $item -> dibsed_by = $userId;
        $item->save();
        return json_encode(array("success" => true, "msg" => "Itemdibsed"));
    }


}
