<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::resource('/api/v1/users', 'UserController');
Route::resource('/api/v1/items', 'ItemController');

Route::get('/api/v1/user/items', 'UserController@items');

Route::post('/register', "UserController@register");
Route::get(
    '/register',
    function () {
        $user = new User;

        return View::make('register_form')->with(array("user" => $user));
    }
);


Route::post('/login', "UserController@browserLogin");
Route::get(
    '/login',
    function () {
        $user = new User;

        return View::make('login_form')->with(array("user" => $user));
    }
);

Route::get('/api/v1/items/get', 'ItemController@show');


Route::get('/api/v1/login', 'UserController@login');

Route::get('/api/v1/logout', 'UserController@logout');
Route::get('/logout', 'UserController@browserLogout');

Route::get("test", function(){
        dd(Auth::check());
    });

Route::get("/wishlist", 'UserController@wishlist');
Route::get("/wishlist/{id}", 'UserController@userWishlist');
Route::post("/dibs", 'ItemController@dibs');

