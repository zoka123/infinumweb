<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Chest Wish</title>
    <link rel="stylesheet" href="{{URL::asset('assets/css/foundation.css')}}" />
    <link rel="stylesheet" href="{{URL::asset('assets/css/custom.css')}}" />
    <link rel="stylesheet" href="{{URL::asset('assets/css/font-awesome.min.css')}}" />
    <link
        href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic&subset=latin,latin-ext'
        rel='stylesheet' type='text/css'>
    <script src="{{URL::asset('assets/js/vendor/modernizr.js')}}"></script>
    @section('header-links')
    @show
</head>
<body>

@section('navbar')
<div class="top-navbar">
    <nav class="row">
        <div class="column text-center small-12 medium-6 large-4">
            <h1>
                <a href="." class="wish-font-large">
                    <img src="{{URL::asset('assets/img/logo-w.png')}}" alt="logo" />
                </a>
            </h1>
        </div>
        <div id="top-buttons" class="small-right small-12 medium-6 large-4 text-right column">
            @if ( Auth::check())
            <a href=" {{ url('/logout') }} "
               class="text-center small-12 right small medium-6 column button btn-red">Logout</a>
            <a href=" {{ url('/wishlist') }} "
               class="text-center small-12 right small medium-6 column button btn-red">My Wishlist</a>
            @else

            <a href=" {{ url('/login') }} " data-reveal-id="myModal" data-reveal-ajax="true"
               class="text-center small-12 small medium-6 right column button btn-red">Login</a>
            <a href="{{ url('/register') }}" data-reveal-id="myModal" data-reveal-ajax="true"
               class="text-center small-12 small medium-6 right column button btn-red">Register</a>
            @endif

        </div>
    </nav>
</div>
@show

@section('header')

@show

@section('before-body')
@show

@section('body')
@show

@section('after-body')
@show

@section('footer')
<footer class="row">
    <div class="large-12 columns">
        <hr />
        <div class="row">

            <div class="large-6 columns">
                <p>© Copyright no one at all. Vuco za predsjednika</p>
            </div>
        </div>
    </div>
</footer>
@show
<!-- End Footer -->

<div id="myModal" class="small reveal-modal" data-reveal>
    <a class="close-reveal-modal">&#215;</a>
</div>

@section('footer-scripts')
<script src="{{URL::asset('assets/js/vendor/jquery.js')}}"></script>
<script src="{{URL::asset('assets/js/foundation.min.js')}}"></script>
<script src="{{URL::asset('assets/js/custom.js')}}"></script>
<script src="{{URL::asset('assets/js/foundation/foundation.reveal.js')}}"></script>
<script>
    $(document).foundation();
</script>
@show

@section('body-end')
</body>
</html>
@show
