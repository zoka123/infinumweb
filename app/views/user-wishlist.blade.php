@extends('../view')

@section('navbar')
@parent
@stop

@section('header')
@parent
@stop

@section('before-body')

@stop

@section('body')
<div class="row">
    <div id="global-msg" class="alert-box" style="display:none"></div>
    <h2 class="small-12 column">Latest wishes</h2>
    @foreach ($items as $i)
    @include('../items/front_item_login', array('item'=>'i'))
    @endforeach
</div>
@stop

@section('after-body')

@stop