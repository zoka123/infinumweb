@extends('view')

@section('navbar')
@parent
@stop

@section('header')
@parent
@stop

@section('before-body')

@stop

@section('body')
<div class="row">
    <div id="global-msg" class="alert-box" style="display:none"></div>
    <h2 class="small-12 column">Latest wishes</h2>
    @foreach ($items as $i)
    @if ( Auth::check() )
    @include('items/front_item_login', array('item'=>'i'))

    @else
    @include('items/front_item', array('item'=>'i'))
    @endif

    @endforeach
</div>
@stop

@section('after-body')

@stop

@section('footer-scripts')
@parent

<script>

    $(function () {

        $("body").on("submit", ".ajaxform", function (e) {
            var postData = $(this).serializeArray();
            var formURL = $(this).attr("action");

            var form = $(this);
            form.find(".error").html("").hide();
            loadingStart(form);

            $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        data = $.parseJSON(data);

                        if (data.success == true) {
                            $('#myModal').foundation('reveal', 'close');
                            $("#global-msg").text(form.attr("data-success-msg"));
                            $("#global-msg").addClass("success").show();
                            loadingStop(form);


                            var refresh = form.attr("data-refresh");
                            if(refresh == "true"){
                                window.location.reload();
                            }

                        } else {
                            var msg = data.msg;
                            //console.log(msg);
                            form.find(".alert").html(msg).show();
                            loadingStop(form);
                        }


                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //if fails
                    }
                });
            e.preventDefault(); //STOP default action
        });

        $(".item-dibs.available").on("click", function(){
            loadingStart($(this));
            var itemId = $(this).attr("data-item-id");
            var request = $.ajax({
                url: "{{ url("/dibs") }}",
                type: "POST",
                data: { id : itemId},
                dataType: "html"
            });

            request.done(function( msg ) {
                console.log(msg);
                msg = $.parseJSON(msg);

                console.log(msg.success);

                if(msg.success == true){
                    window.location.reload();
                }
            });

            request.fail(function( jqXHR, textStatus ) {
                //alert( "Request failed: " + textStatus );

            });

        })



    });


</script>

@stop