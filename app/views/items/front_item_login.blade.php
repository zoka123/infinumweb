<div class="front-item column medium-4 large-4">
    <div>
        <div class="item-bg transition" style="background:#000 url(' {{ $i->image }}  ');"></div>
        <h2 class="gradient-down item-title">{{ $i -> name }}</h2>

        <div class="gradient-up  item-description clearfix">
            <div class="item-user">

            <a href="{{ url('wishlist/'. $i->userId) }}">    <i class="fa fa-user"></i> {{ $i -> username }}</a>

            </div>
            <div class="item-text"><i class="fa fa-comment"></i>  {{ $i -> description }}</div>
            <div class="item-date"> <i class="fa fa-clock-o"></i> {{ date("d.m.Y H:i",strtotime($i -> created_at)) }}</div>

            @if( !$i->dibsed_by )
            <div class="item-dibs available transition" data-item-id="{{ $i -> itemId }}"><i class="fa fa-star-o fa-spin"></i> Dibs!</div>
            @else
            <div class="item-dibs transition"><i class="fa fa-star"></i> Dibsed!</div>
            @endif

        </div>
    </div>
</div>