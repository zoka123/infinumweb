<a class="close-reveal-modal">&#215;</a>
{{ Form::open(array('url' => '/register', 'method' => 'post', "class"=>"ajaxform", "data-success-msg" => "User registered", "data-refresh" => "false")) }}
<div class="alert alert-box" style="display:none"></div>
{{ Form::token(); }}

{{ Form::label('name', 'First and last name') }}
{{ Form::text('name', null, array("required" => "required")) }}

{{ Form::label('email', 'Email') }}
{{ Form::email('email', null, array("required" => "required")) }}

{{ Form::label('password', 'Password') }}
{{ Form::password('password', null, array("required" => "required")) }}

{{ Form::label('password_confirmation', 'Password Confirmation') }}
{{ Form::password('password_confirmation', null, array("required" => "required")) }}



{{Form::submit('Register', array("class"=>"button success expand"));}}

{{ Form::close() }}