<a class="close-reveal-modal">&#215;</a>
{{ Form::open(array('url' => '/login', 'method' => 'post', "class"=>"ajaxform", "data-success-msg" => "Login succeeded", "data-refresh"=>"true")) }}
<div class="alert alert-box" style="display:none"></div>
{{ Form::token(); }}

{{ Form::label('email', 'Email') }}
{{ Form::email('email', null, array("required" => "required")) }}

{{ Form::label('password', 'Password') }}
{{ Form::password('password', null, array("required" => "required")) }}

{{Form::submit('Login', array("class"=>"button success expand"));}}

{{ Form::close() }}