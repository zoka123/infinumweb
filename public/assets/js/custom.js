function loadingStart(elem) {
    elem.append('<div class="form-loader text-center pink-color"><i class="fa fa-spinner fa-spin fa-3x"></i></div>');
}

function loadingStop(elem) {
    elem.find('.form-loader').remove();
}